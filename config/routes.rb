Eenge::Application.routes.draw do
  
  resources :album_de_fotos
  
  resources :fotos
  
  devise_for :users

  resources :inscricaos, :only => [:new, :create, :update, :edit]
  
  resources :users do
    post 'confirmar', :on => :member
    post 'desfazer', :on => :member
  end
  
  resources :contatos, :only => [:new, :create]
  
  root :to => 'pages#home'
  
  match '/patrocinio', :to => 'pages#sponsors', :via => :get
  match '/sobre', :to => 'pages#sobre', :via => :get
  match '/programacao', :to => 'pages#programacao', :via => :get
  match '/usuario', :to => 'users#show', :via => :get
  match '/localizacao', :to => 'pages#localizacao', :via => :get
  match '/contato', :to => 'pages#contato', :via => :get
  match '/cronograma', :to => 'pages#cronograma', :via => :get
  match '/minicursos', :to => 'pages#minicursos', :via => :get
  match '/coquetel', :to => 'pages#coquetel', :via => :get
  match '/visitas_tecnicas', :to => 'pages#visitas_tecnicas', :via => :get
  match '/como_chegar', :to => 'pages#como_chegar', :via => :get
  match '/aonde_ficar', :to => 'pages#aonde_ficar', :via => :get
  match '/ficha_cadastral', :to => 'users#ficha', :via => :get
  match '/pagamento', :to => 'pages#pagamento', :via => :get
  match '/congresso_pmi', :to => 'pages#congresso_pmi', :via => :get
  match '/efetuado', :to => 'pages#efetuado'  
  
  namespace :user do
    root :to => 'users#show'
  end
  
  resources :noticias, :except => [:index]
  resources :patrocinadores
    
  # The priority is based upon order of creation:
  # first created -> highest priority.

  # Sample of regular route:
  #   match 'products/:id' => 'catalog#view'
  # Keep in mind you can assign values other than :controller and :action

  # Sample of named route:
  #   match 'products/:id/purchase' => 'catalog#purchase', :as => :purchase
  # This route can be invoked with purchase_url(:id => product.id)

  # Sample resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Sample resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Sample resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Sample resource route with more complex sub-resources
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', :on => :collection
  #     end
  #   end

  # Sample resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end

  # You can have the root of your site routed with "root"
  # just remember to delete public/index.html.
  # root :to => "welcome#index"

  # See how all your routes lay out with "rake routes"

  # This is a legacy wild controller route that's not recommended for RESTful applications.
  # Note: This route will make all actions in every controller accessible via GET requests.
  # match ':controller(/:action(/:id(.:format)))'
end
