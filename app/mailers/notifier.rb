#coding: utf-8

class Notifier < ActionMailer::Base
  default :from => "perfileenge@gmail.com"
  
  def confirmacao(user)
      @user = user
      @url = "http://www.eenge.com.br/producao"
      mail(:to => user.email, :subject => "Sua inscrição no 12o EENGE foi confirmada!")
  end
  
  def cancelamento(user)
    @user = user
    @url = "http://www.eenge.com.br/producao"
    mail(:to => user.email, :subject => "Sua confirmação de pagamento do 12o EENGE foi cancelada.")
  end
  
  def contato(user)
    @user = user
    @url = "http://www.eenge.com.br/producao"
    mail(:to => "eengeciv@gmail.com",
         :from => @user.email,
         :subject => "Novo contato do site por #{@user.email}")
  end
  
  def inscricao_criada(user)
    @user = user
    @url = "http://www.eenge.com.br/producao"    
    mail(:to => user.email, :subject => "Recebemos sua inscrição no 12o EENGE.")
  end
  
end

