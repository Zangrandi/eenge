class PatrocinadoresCell < Cell::Rails

  def show
    @patrocinadores = Patrocinador.all(:order => "tipo")
    @apoio = Patrocinador.find_all_by_tipo(:apoio)
    @user = options[:user]
    
    render
  end
  
end
