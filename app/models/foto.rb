class Foto < ActiveRecord::Base
  belongs_to :album_de_foto
  
  validates :pic, :album_de_foto_id, :presence => true
  has_attached_file :pic
  
end
