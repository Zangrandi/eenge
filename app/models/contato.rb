class Contato
  include ActiveModel::Validations
  
  validates_presence_of :email, :sender_name, :mensagem
  
  attr_accessor :id, :email, :sender_name, :mensagem
  
  def initialize(attributes = {})
    attributes.each do |key, value|
      self.send("#{key}=", value)
    end
    
    @attributes = attributes
  end
  
  def read_attributes_for_validation(key)
    @attributes[key]
  end
  
  def to_key
  end
  
  def save
    if self.valid?
      Notifier.contato(self).deliver
      return true
    end
    return false
  end
end