class Inscricao < ActiveRecord::Base
  validates :data_inicio, :data_termino, :presence => true
  
  def self.aberta?
    find(1).data_inicio < Time.now && find(1).data_termino > Time.now
  end
end
