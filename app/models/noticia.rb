class Noticia < ActiveRecord::Base
  
  validates :corpo, :secao, :presence => true
  
  scope :da_secao, lambda {|secao| 
    where("noticias.secao = ?", secao)
  }
  
end
