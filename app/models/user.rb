#coding: utf-8

class User < ActiveRecord::Base
  after_create :send_email
  # Include default devise modules. Others available are:
  # :token_authenticatable, :encryptable, :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  
  # Setup accessible (or protected) attributes for your model
  attr_accessible :instituicao, :tipo_inscricao, :nome, :sexo, :data_nascimento, :cpf, 
                  :logradouro, :bairro, :cidade, :estado, :cep, :telefone, 
                  :email, :password, :password_confirmation, :remember_me, :status_inscricao
                  
  validates :nome, :sexo, :data_nascimento, :cpf, :logradouro, :bairro, 
            :cidade, :estado, :cep, :telefone, :tipo_inscricao, :presence => true
            
  validates :cpf, :uniqueness => true
  
  validates_presence_of :instituicao, :if => :nao_uenf?

  
  usar_como_cpf :cpf
  
  has_many :eventos
  
  def primeiro_nome
    nome.split(' ').first
  end
  
  def sobrenome
    nome.split(' ').last
  end
  
  def numero_inscricao
    498 + id
  end

  def preco_inscricao
    tipo_inscricao.split("R$").second
  end
  
  def tipo_da_inscricao
    tipo_inscricao.split("R$").first
  end
  
  def confirmado?
    status_inscricao == "Confirmada"
  end
  
  def inscricao_pagavel?
    (["Confirmada", "Aguardando confirmação do pagamento no PagSeguro", "Pagamento em analise pelo PagSeguro"].include? status_inscricao) ? false : true    
  end

  def estudante_uenf?
    tipo_da_inscricao == "Estudante UENF "    
  end
  
  def nao_uenf?
    tipo_da_inscricao != "Estudante UENF "
  end
  
  def voluntario_pmi?
    tipo_da_inscricao == "Voluntário PMI "
  end
  
  def estudante_externo?
    tipo_da_inscricao == "Estudante Externo "
  end
  
  def outros?
    tipo_da_inscricao == "Outros "
  end

  def send_email
    Notifier.inscricao_criada(self).deliver
  end
  
end
