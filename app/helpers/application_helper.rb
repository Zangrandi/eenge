#coding: utf-8

module ApplicationHelper
  
  def banner
    image_tag("banner3.png", :size => "787x225", :alt => "EENGE")
  end
  
  def pagseguro
    "https://pagseguro.uol.com.br/"
  end
  
  def curso
    "Engenharia de Produção"
  end
  
  def title
    default = "12º EENGE #{curso}"
    @titulo.present? ? "#{@titulo} | #{default}" : default
  end
  
  def albuns
    AlbumDeFoto.all.map do |a| 
      {a.nome => a.id}
    end
  end
  
  def estados
    ["AC", "AL", "AP", "AM", "BA", "CE", "DF", "ES", "GO", 
      "MA", "MT", "MS", "MG", "PA", "PB", "PR", "PE", "PI", 
      "RJ", "RN", "RS", "RO", "RR", "SC", "SP", "SE", "TO"]
  end
  
  def secoes
    ["Home", "Sobre o Evento", "Cronograma", "Minicursos", "Palestras", "Visitas Técnicas",
      "Como Chegar", "Aonde Ficar", "Congresso PMI"]
  end
    
  def tipos_de_inscricao
    ["Estudante UENF R$ 43,00", "Estudante Externo R$ 43,00", "Voluntário PMI R$ 43,00", "Outros R$ 50,00"]
  end
  
  
end
