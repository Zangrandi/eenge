class FotosController < ApplicationController
  def show
    @foto = Foto.find(params[:id])
    @album = AlbumDeFoto.find(@foto.album_de_foto_id)

    @fotos = @album.fotos

    render :layout => "foto"
  end

  def new
    @foto = Foto.new
  end

  def edit
    @foto = Foto.find(params[:id])
  end

  def create
    @foto = Foto.new(params[:foto])

    if @foto.save
      redirect_to @foto
    else
      render :action => "new"
    end

  end

  def update
    @foto = Foto.find(params[:id])

    if @foto.update_attributes(params[:foto])
      redirect_to @foto
    else
      render :action => "edit"
    end
  end

  def destroy
    @foto = Foto.find(params[:id])
    @foto.destroy

    redirect_to(album_de_fotos_url)
  end
end
