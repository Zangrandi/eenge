#coding: utf-8

class ApplicationController < ActionController::Base
  protect_from_forgery
  before_filter :set_p3p    
    
  def after_sign_in_path_for(resource)
    stored_location_for(resource) || usuario_path
  end
  
  def set_p3p
     response.headers["P3P"]='CP="CAO PSA OUR"'
  end
end
