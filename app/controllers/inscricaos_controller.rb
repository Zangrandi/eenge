class InscricaosController < ApplicationController
  def new
    @inscricao = (Inscricao.any? ? Inscricao.find(1) : Inscricao.new)
  end

  def create
    @inscricao = (Inscricao.any? ? Inscricao.find(1) : Inscricao.new(params[:inscricao]) )
    
    if @inscricao.save
      redirect_to usuario_path, :notice => "Inscricao marcada"
    else
      redirect_to usuario_path, :notice => "Ocorreu algum erro"
    end
  end
  
  def update
    @inscricao = Inscricao.find(1)
  
    if @inscricao.update_attributes(params[:inscricao])
      redirect_to(usuario_path, :notice => 'Data atualizada com sucesso.')
    else
      render :action => "edit"
    end
  end
  
  def destroy
    @inscricao = Inscricao.find(1)
    @inscricao.destroy
  end
    

end