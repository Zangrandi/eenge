class PatrocinadoresController < ApplicationController
  before_filter :authenticate_admin
  
  def new
    @patrocinador = Patrocinador.new
  end
  
  def create
    @patrocinador = Patrocinador.new(params[:patrocinador])
  
    if @patrocinador.save
      redirect_to(root_path, :notice => "Patrocinador adicionado com sucesso.")
    else
      redirect_to(root_path, :notice => "Algo deu errado.")
    end
  end
  
  def index
    @patrocinadores = Patrocinador.all
  end  

  def destroy
    @patrocinador = Patrocinador.find(params[:id])
    @patrocinador.destroy
    
    redirect_to root_path
  end
  
  protected
  def authenticate_admin
    redirect_to root_path if !current_user.try(:admin)
  end
end