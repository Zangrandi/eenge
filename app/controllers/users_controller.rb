#coding: utf-8

class UsersController < ApplicationController
  before_filter :authenticate_user!
  
  def show
    redirect_to root_path    if current_user.try(:admin)
  end
  
  def index
    redirect_to root_path if !current_user.try(:admin) 
    @users = User.find_all_by_admin("false", :order => "created_at")
    
    @users_confirmados = @users.select {|user| user.status_inscricao == "Confirmada" }
    @users_pendentes = @users.select {|user| user.status_inscricao != "Confirmada" }

    respond_to do |format|
      format.html { render :layout => "index" }
      format.xls { send_data @users.to_xls}
    end
  end
  
  def dados
  end
  
  def ficha
    if current_user.status_inscricao != "Confirmada"
      redirect_to root_path 
    else
      render :layout => 'ficha'      
    end

  end
  
  def confirmar
    @user = User.find(params[:id])
    @user.status_inscricao = "Confirmada"
    
    if @user.save
      Notifier.confirmacao(@user).deliver
      redirect_to users_path
    else
      redirect_to users_path, :notice => "Algo deu errado"  
    end
  end
  
  def desfazer
    @user = User.find(params[:id])
    @user.status_inscricao = "Aguardando Pagamento"
    
    if @user.save
      Notifier.cancelamento(@user).deliver      
      redirect_to users_path
    else
      redirect_to users_path, :notice => "Algo deu errado"  
    end
  end   
  
  
  def edit 
    @user = User.find(params[:id])
  end
  
  def update
    @user = User.find(params[:id])    

    if @user.update_attributes(params[:user])
      redirect_to(users_path, :notice => 'User editado com sucesso.')
    else
      render :action => "edit"
    end
  end  
end