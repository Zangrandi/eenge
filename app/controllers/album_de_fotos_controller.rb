class AlbumDeFotosController < ApplicationController
  def index
    @album_de_fotos = AlbumDeFoto.all
  end

  def show
    @album_de_foto = AlbumDeFoto.find(params[:id])
  end

  def new
    @album_de_foto = AlbumDeFoto.new
  end

  def edit
    @album_de_foto = AlbumDeFoto.find(params[:id])
  end

  def create
    @album_de_foto = AlbumDeFoto.new(params[:album_de_foto])

    if @album_de_foto.save
      redirect_to album_de_fotos_path
    else
      render :action => "new"
    end
  end

  def update
    @album_de_foto = AlbumDeFoto.find(params[:id])

    if @album_de_foto.update_attributes(params[:album_de_foto])
      redirect_to album_de_fotos_path
    else
      render :action => "edit"
    end
  end

  def destroy
    @album_de_foto = AlbumDeFoto.find(params[:id])
    @album_de_foto.destroy

    redirect_to(album_de_fotos_url)
  end
end
