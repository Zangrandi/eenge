# coding: utf-8

class PagesController < ApplicationController
  skip_before_filter :verify_authenticity_token
    
  def home
    @noticias = Noticia.find_all_by_secao("Home", :order => "created_at DESC")
    @noticia_principal = Noticia.find_by_secao("PRINCIPAL")
  end
  
  def sobre
    @secao = "Sobre o Evento"
    @noticia = Noticia.find_by_secao(@secao)    
    @titulo = @secao
    
    render :template => 'pages/noticia_template'    
  end

  def cronograma
    @secao = "Cronograma"
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = @secao
    render :template => 'pages/noticia_template'    
  end
  
  def minicursos
    @secao = "Minicursos"
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = @secao
    render :template => 'pages/noticia_template'    
  end
  
  def coquetel
    @secao = "Coquetel"
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = @secao
    render :template => 'pages/noticia_template'        
  end
  
  def visitas_tecnicas
    @secao = "Visitas Tecnicas"
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = @secao
    render :template => 'pages/noticia_template'        
  end
    
  def como_chegar
    @secao = "Como Chegar"
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = @secao
  end
  
  def aonde_ficar
    @secao = "Aonde Ficar"    
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = @secao
    render :template => 'pages/noticia_template'    
  end
  
  def congresso_pmi
    @secao = "Congresso PMI"
    @noticia = Noticia.find_by_secao(@secao)
    @titulo = "Congresso PMI Rio"
    render :template => 'pages/noticia_template'
  end
  
  def contato
    @titulo = "Contato"
  end
  
  def efetuado

  end
end