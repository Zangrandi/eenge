# coding: utf-8

class NoticiasController < ApplicationController
  before_filter :authenticate_admin, :except => :show
  
  def new
    @noticia = Noticia.new(:secao => params[:secao])
  end
  
  def create
    @noticia = Noticia.new(params[:noticia])
    
    if @noticia.save
      redirect_to(@noticia, :notice => "Notícia criada com sucesso.")
    else
      render :action => "new"
    end
  end
  
  def show
    @noticia = Noticia.find(params[:id])
  end
  
  def edit
    @noticia = Noticia.find(params[:id])
  end
  
  def update
    @noticia = Noticia.find(params[:id])

    if @noticia.update_attributes(params[:noticia])
      redirect_to(@noticia, :notice => 'Notícia atualizada com sucesso.')
    else
      render :action => "edit"
    end
  end
  
  def destroy
    @noticia = Noticia.find(params[:id])
    @noticia.destroy
    
    redirect_to root_path
  end  
    
  protected
  def authenticate_admin
    redirect_to root_path if !current_user.try(:admin)
  end
end