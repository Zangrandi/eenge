#coding: utf-8
class ContatosController < ApplicationController
  
  def new
    @contato = Contato.new(:id => 1)
  end
  
  def create
    @contato = Contato.new(params[:contato])
    
    if @contato.save
      redirect_to('/', :notice => "Email enviado.")
    else
      flash[:alert] = "Você precisa preencher todos os campos"
      render 'new'
    end
  end

end