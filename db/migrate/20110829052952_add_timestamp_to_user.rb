class AddTimestampToUser < ActiveRecord::Migration
  def self.up
    create_table :eventos do |t|
      t.string :nome
      t.belongs_to :user
      t.datetime :data
    end
  end

  def self.down
    drop_table :eventos
  end
end
