class AddNInscricaoToUsers < ActiveRecord::Migration
  def self.up
    add_column :users, :numero_inscricao, :integer
  end

  def self.down
    remove_column :users, :numero_inscricao
  end
end
