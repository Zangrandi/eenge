class AddTipoToPatrocinadores < ActiveRecord::Migration
  def self.up
    add_column :patrocinadores, :tipo, :string
  end

  def self.down
    remove_column :patrocinadores, :tipo, :string    
  end
end
