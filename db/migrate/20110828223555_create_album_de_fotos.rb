class CreateAlbumDeFotos < ActiveRecord::Migration
  def self.up
    create_table :album_de_fotos do |t|
      t.string :nome
      
      t.timestamps
    end
  end

  def self.down
    drop_table :album_de_fotos
  end
end
