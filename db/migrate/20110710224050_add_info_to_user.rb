class AddInfoToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :sexo, :string
    add_column :users, :logradouro, :string
    add_column :users, :bairro, :string
    add_column :users, :cep, :string
    remove_column :users, :rg
  end

  def self.down
    remove_column :users, :sexo
    remove_column :users, :logradouro
    remove_column :users, :bairro
    remove_column :users, :cep
    add_column :users, :rg, :string
  end
end
