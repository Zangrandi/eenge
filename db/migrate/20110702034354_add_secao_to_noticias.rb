class AddSecaoToNoticias < ActiveRecord::Migration
  def self.up
    add_column :noticias, :secao, :string
  end

  def self.down
    remove_column :noticias, :secao
  end
end
