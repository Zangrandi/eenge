class AddStatusInscricaoToUser < ActiveRecord::Migration
  def self.up
    add_column :users, :status_inscricao, :string, :default => "Aguardando Pagamento"
    add_column :users, :tipo_inscricao, :string
  end

  def self.down
    remove_column :users, :status_inscricao
    remove_column :users, :tipo_inscricao
  end
end
