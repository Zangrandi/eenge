class CreateFotos < ActiveRecord::Migration
  def self.up
    create_table :fotos do |t|
      t.string :legenda
      
      t.string :pic_file_name
      t.string :pic_content_type
      t.integer :pic_file_size
      t.datetime :pic_updated_at
      
      t.belongs_to :album_de_foto
      t.timestamps
    end
  end

  def self.down
    drop_table :fotos
  end
end
