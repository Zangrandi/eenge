class AdicionarLinkImagemAoPatrocinador < ActiveRecord::Migration
  def self.up
    add_column :patrocinadores, :link_imagem, :string
  end

  def self.down
    remove_column :patrocinadores, :link_imagem
  end
end
