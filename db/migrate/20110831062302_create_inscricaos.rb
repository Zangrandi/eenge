class CreateInscricaos < ActiveRecord::Migration
  def self.up
    create_table :inscricaos do |t|
      t.datetime :data_inicio
      t.datetime :data_termino
      t.timestamps
    end
  end

  def self.down
    drop_table :inscricaos
  end
end
