# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20111026192436) do

  create_table "album_de_fotos", :force => true do |t|
    t.string   "nome"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "eventos", :force => true do |t|
    t.string   "nome"
    t.integer  "user_id"
    t.datetime "data"
  end

  create_table "fotos", :force => true do |t|
    t.string   "legenda"
    t.string   "pic_file_name"
    t.string   "pic_content_type"
    t.integer  "pic_file_size"
    t.datetime "pic_updated_at"
    t.integer  "album_de_foto_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "inscricaos", :force => true do |t|
    t.datetime "data_inicio"
    t.datetime "data_termino"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "noticias", :force => true do |t|
    t.string   "titulo"
    t.text     "corpo"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "secao"
  end

  create_table "patrocinadores", :force => true do |t|
    t.string   "nome"
    t.string   "site"
    t.string   "imagem_file_name"
    t.string   "imagem_content_type"
    t.integer  "imagem_file_size"
    t.datetime "imagem_updated_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "tipo"
    t.string   "link_imagem"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                                 :default => "",                     :null => false
    t.string   "encrypted_password",     :limit => 128, :default => "",                     :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",                         :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "nome"
    t.string   "cpf"
    t.string   "cidade"
    t.string   "estado"
    t.string   "instituicao"
    t.string   "telefone"
    t.date     "data_nascimento"
    t.boolean  "admin",                                 :default => false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "sexo"
    t.string   "logradouro"
    t.string   "bairro"
    t.string   "cep"
    t.string   "status_inscricao",                      :default => "Aguardando Pagamento"
    t.string   "tipo_inscricao"
    t.integer  "numero_inscricao"
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

end
