require 'test_helper'

class AlbumDeFotosControllerTest < ActionController::TestCase
  setup do
    @album_de_foto = album_de_fotos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:album_de_fotos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create album_de_foto" do
    assert_difference('AlbumDeFoto.count') do
      post :create, :album_de_foto => @album_de_foto.attributes
    end

    assert_redirected_to album_de_foto_path(assigns(:album_de_foto))
  end

  test "should show album_de_foto" do
    get :show, :id => @album_de_foto.to_param
    assert_response :success
  end

  test "should get edit" do
    get :edit, :id => @album_de_foto.to_param
    assert_response :success
  end

  test "should update album_de_foto" do
    put :update, :id => @album_de_foto.to_param, :album_de_foto => @album_de_foto.attributes
    assert_redirected_to album_de_foto_path(assigns(:album_de_foto))
  end

  test "should destroy album_de_foto" do
    assert_difference('AlbumDeFoto.count', -1) do
      delete :destroy, :id => @album_de_foto.to_param
    end

    assert_redirected_to album_de_fotos_path
  end
end
